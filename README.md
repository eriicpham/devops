#Docker

https://hub.docker.com/repository/docker/vanericpham/ubuntu/general

_____________________________________________________________________________________________________________________________

# Principes de base de Terraform

## Introduction

Ce fichier présente les commandes et les étapes pour mettre en place Terraform dans GCP.

## Configuration Terraform

### Options de la commande Terraform

Pour lister les options de la commande Terraform, vous pouvez exécuter `terraform` dans votre terminal.

### Création du fichier `instance.tf`

Pour créer le fichier `instance.tf`, exécutez la commande suivante dans votre répertoire de travail :

```bash
touch instance.tf
```

Le contenu de `instance.tf` sera décrit ci-dessous.

### Contenu du fichier `instance.tf`

Le fichier `instance.tf` contiendra la configuration nécessaire pour créer une instance Google Compute Engine avec les spécifications suivantes :

- Nom de projet : `NOM PROJET`
- Nom de l'instance : `terraform`
- Type de machine : `e2-medium`
- Zone : `europe-west1-d`
- Disque d'amorçage : utilisant l'image `debian-cloud/debian-11`
- Interface réseau : utilisant le réseau par défaut avec une configuration d'accès par défaut

```hcl
provider "google" {
  project = "NOM PROJET"
  region  = "europe-west1"
}

resource "google_compute_instance" "terraform_instance" {
  name         = "terraform"
  machine_type = "e2-medium"
  zone         = "europe-west1-d"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }
}
```

Assurez-vous de remplacer `<PROJECT_ID>` par l'ID de votre projet Google Cloud.

### Étapes d'exécution Terraform

1. **Initialisation Terraform** : Téléchargez et installez le binaire du fournisseur Google Cloud Platform en exécutant la commande suivante :
   
   ```bash
   terraform init
   ```

2. **Plan d'exécution** : Générez un plan détaillant les actions que Terraform prendra pour atteindre l'état désiré défini dans vos fichiers de configuration en exécutant :
   
   ```bash
   terraform plan
   ```

3. **Application des changements** : Appliquez les changements décrits dans vos fichiers de configuration Terraform en exécutant :
   
   ```bash
   terraform apply
   ```

4. **Inspection de l'état actuel** : Pour afficher les détails des ressources créées et gérées par Terraform, y compris leurs attributs et configurations actuels, utilisez la commande :
   
   ```bash
   terraform show
   ```

## Conclusion

Une fois que vous avez suivi ces étapes, vous aurez déployé avec succès une instance Google Compute Engine en utilisant Terraform sur Google Cloud Platform.

_____________________________________________________________________________________________________________________________

# Bases de la mise en réseau

Ce fichier présente les commandes et les étapes pour gérer les réseaux personnalisés dans GCP.

## Authentification et Configuration

- `gcloud auth list`: Affiche les comptes Google Cloud actuellement authentifiés sur votre machine.
- `gcloud config list project`: Affiche l'identifiant du projet Google Cloud.

## Création d'un Réseau Personnalisé

1. **Créer un Réseau Personnalisé**: Exécutez la commande suivante dans Cloud Shell :
   ```bash
   gcloud compute networks create taw-custom-network --subnet-mode custom
   ```

2. **Créer un Sous-Réseau dans la Région Europe-West1**:
   ```bash
   gcloud compute networks subnets create subnet-europe-west1 
   --network taw-custom-network 
   --region europe-west1 
   --range 10.0.0.0/16
   ```
  
3. **Créer d'autres Sous-Réseaux**:
   Utilisez la commande similaire pour créer des sous-réseaux dans d'autres régions avec des plages d'adresses IP spécifiques.

4. **Afficher la liste des réseaux**
   ```bash
   gcloud compute networks subnets list \
   --network taw-custom-network
  ```
## Gestion des Règles de Pare-feu

- **Créer une règle autorisant le trafic HTTP**:
  ```bash
  gcloud compute firewall-rules create nw101-allow-http 
  --allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 
  --target-tags http
  ```

- **Créer d'autres règles de pare-feu**:
  Vous pouvez créer des règles pour autoriser différents types de trafic, tels que ICMP, SSH ou RDP, en fonction de vos besoins.

## Création d'Instances

- **Créer une instance dans la zone Europe-West1-D**:
  ```bash
  gcloud compute instances create us-test-01 
  --subnet subnet-europe-west1 
  --zone europe-west1-d 
  --machine-type e2-standard-2 
  --tags ssh,http,rules
  ```

- **Créer d'autres instances dans différentes zones**:
  Utilisez des commandes similaires pour créer des instances dans différentes zones avec des sous-réseaux spécifiques et d'autres configurations.

## Vérification et Test

- **Vérification de la Connectivité**:
  Après avoir configuré les instances et règles de pare-feu, assurez-vous de tester la connectivité en utilisant des commandes telles que `ping` vers les adresses IP externes des instances.
